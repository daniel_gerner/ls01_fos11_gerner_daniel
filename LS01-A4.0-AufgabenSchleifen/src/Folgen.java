import java.util.Scanner;

public class Folgen {

	public static void main(String[] args) {
		zeahlenA();
		zeahlenB();

	}

	public static void zeahlenA() {
		Scanner tastatur = new Scanner(System.in);

		int n;
		int i;
		
		System.out.println("Bitte geben Sie Ihre Zahl ein: ");
		n = tastatur.nextInt();

		for (i = 1; i <= n; i++) {
			System.out.print(i+" ");
		}
	}

	public static void zeahlenB() {
		Scanner tastatur = new Scanner(System.in);

		int n;
		int i;
		System.out.println();
		System.out.println("Bitte geben Sie Ihre Zahl ein: ");
		n = tastatur.nextInt();

		for (i = 1; i <= n; n--) {
			System.out.print(n + " ");
		}

	}

}