
public class Ausgabenbeispiel {

	public static void main(String[] args) {
		
		// Kommerzahl 
		System.out.printf(" Zahl: %f-20.1f %n", 12345.6789);
		
		// Gansezahlen
		System.out.printf(" Zahl: %-20d %n", 123456789);
		
		// Zeichenketten (String)
		System.out.printf(" Wort: %-20.4s   ", "abcdef");

	}

}
